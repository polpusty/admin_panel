from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', view=views.ModelsListView.as_view(), name='index'),
    url(r'^model/(?P<model>.*)/list$', view=views.ModelListView.as_view(), name='model-list'),
    url(r'^model/(?P<model>.*)/create', view=views.ModelCreateView.as_view(), name='model-create'),
    url(r'^model/(?P<model>.*)/(?P<id>.*)/detail', view=views.ModelDetailView.as_view(), name='model-detail'),
    url(r'^model/(?P<model>.*)/(?P<id>.*)/update', view=views.ModelUpdateView.as_view(), name='model-update'),
    url(r'^model/(?P<model>.*)/(?P<id>.*)/delete', view=views.ModelDeleteView.as_view(), name='model-delete'),
]
