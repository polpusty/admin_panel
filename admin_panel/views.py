from django.views.generic import ListView, CreateView, DetailView, UpdateView, DeleteView
from django.http import Http404
from django.forms import modelform_factory
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from .registermodels import RegisterModels

from example_app.models import Owner, Car

register_models = RegisterModels()
register_models.register(Owner)
register_models.register(Car)


class LoginRequiredMixin(object):
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(LoginRequiredMixin, self).dispatch(request, *args, **kwargs)


class ModelsListView(LoginRequiredMixin, ListView):
    queryset = register_models
    template_name = 'admin_panel/models_list.html'


class ModelListView(LoginRequiredMixin, ListView):
    template_name = 'admin_panel/model_list.html'

    def get_context_data(self, **kwargs):
        context = super(ModelListView, self).get_context_data(**kwargs)
        context['model_name'] = self.kwargs['model']
        return context

    def get_queryset(self):
        model = register_models.get_model(self.kwargs['model'])
        if model is None:
            raise Http404()
        return model.objects.all()


class ModelCreateView(LoginRequiredMixin, CreateView):
    template_name = 'admin_panel/model_form.html'

    def get_form(self, form_class=None):
        model = register_models.get_model(self.kwargs['model'])
        if model is None:
            raise Http404()
        return modelform_factory(model, exclude=())

    def post(self, request, *args, **kwargs):
        form = self.get_form()(request.POST)
        if form.is_valid():
            form.save()
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super(ModelCreateView, self).get_context_data(**kwargs)
        context['model_name'] = self.kwargs['model']
        return context

    def get_success_url(self):
        return reverse('admin_panel:model-list', args=(self.kwargs['model'],))


class ModelDetailView(LoginRequiredMixin, DetailView):
    template_name = 'admin_panel/model_detail.html'

    def get_object(self, queryset=None):
        model = register_models.get_model(self.kwargs['model'])
        if model is None:
            raise Http404()
        obj = model.objects.get(pk=self.kwargs['id'])
        if obj is None:
            raise Http404()
        return obj


class ModelUpdateView(LoginRequiredMixin, UpdateView):
    data = None
    template_name = 'admin_panel/model_form.html'

    def get_form(self, form_class=None):
        model = register_models.get_model(self.kwargs['model'])
        if model is None:
            raise Http404()
        return modelform_factory(model, exclude=())(instance=self.object, data=self.data)

    def get_object(self, queryset=None):
        model = register_models.get_model(self.kwargs['model'])
        if model is None:
            raise Http404()
        obj = model.objects.get(pk=self.kwargs['id'])
        if obj is None:
            raise Http404()
        return obj

    def get_success_url(self):
        return reverse('admin_panel:model-list', args=(self.kwargs['model'],))

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.data = request.POST
        form = self.get_form()
        if form.is_valid():
            form.save()
            return self.form_valid(form)
        else:
            return self.form_invalid(form)


class ModelDeleteView(LoginRequiredMixin, DeleteView):
    template_name = 'admin_panel/model_delete.html'

    def get(self, *args, **kwargs):
        return self.post(*args, **kwargs)

    def get_object(self, queryset=None):
        model = register_models.get_model(self.kwargs['model'])
        if model is None:
            raise Http404()
        obj = model.objects.get(pk=self.kwargs['id'])
        if obj is None:
            raise Http404()
        return obj

    def get_success_url(self):
        return reverse('admin_panel:model-list', args=(self.kwargs['model'],))
