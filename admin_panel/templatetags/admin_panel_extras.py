from django import template

register = template.Library()


@register.filter
def class_name(cls):
    return cls.__class__.__name__


@register.filter
def object_atrributes(obj):
    return [(key, getattr(obj, key, '')) for key in obj._meta.get_all_field_names()]
