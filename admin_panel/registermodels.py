from django.db.models import Model

class RegisterModels(object):
    def __init__(self):
        self._models = []

    def __getitem__(self, item):
        return self._models[item]

    def get_model(self, name):
        return {model.__name__: model for model in self._models}.get(name, None)

    def register(self, model):
        if issubclass(model, Model) and model not in self._models:
            self._models.append(model)
        else:
            raise TypeError