from django.db import models


class Owner(models.Model):
    name = models.CharField(max_length=240)
    surname = models.CharField(max_length=240)
    pesel = models.CharField(max_length=11, unique=True)

    def __str__(self):
        return "Owner: %s %s - %s" % (self.name, self.surname, self.pesel)


class Car(models.Model):
    brand = models.CharField(max_length=240)
    model = models.CharField(max_length=240)
    owner = models.ForeignKey('Owner')

    def __str__(self):
        return "Car: %s %s - %s %s" % (self.brand, self.model, self.owner.name, self.owner.surname)
